#NoEnv

SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
DetectHiddenWindows On
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1

WinActivate, ahk_class Shell_TrayWnd
WinSet, Transparent, Off
WinGetPos, trayX, trayY, trayWidth, trayHeight, A
iconPath = C:\development\BoltMacroRecording\BoltMacroRecording\template.bmp
startSearch = %trayWidth%*(3/4)
if ErrorLevel > 0
	MsgBox Early problem
ImageSearch, FoundX, FoundY, 0, 0, startSearch, %trayHeight%, %iconPath%
if ErrorLevel = 2
	MsgBox Could not conduct the search
else if ErrorLevel = 1
	MsgBox Icon couldn't be found
else {
	MouseClick, Right, %FoundX%+4, %FoundY%+2, 1, 0
	Sleep, 40
	Send, {Down}
	Sleep, 3
	Send, {Down}
	Sleep, 3
	Send, {Down}
	Sleep, 3
	Send, {Enter}
}
Return