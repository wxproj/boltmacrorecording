﻿; This script was created using Pulover's Macro Creator

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode RegEx
DetectHiddenWindows On
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1

startRecordingMacro:
; Look for window talking about admin privileges
IfWinExist, ahk_class AutoHotkeyGUI, Resources.dll
{
	WinActivate, ahk_class AutoHotkeyGUI, Resources.dll
	Sleep, 30
	Send, {Enter}
	Sleep, 20
}
; Look for the Welcome window
WinWaitActive, ahk_class AutoHotkeyGUI, Welcome to Pulover's Macro Creator, 2
if ErrorLevel {
	Return
}
ControlClick, OK
Sleep, 10
; Look for the begging window for up to 3 seconds
WinWaitActive, ahk_class AutoHotkeyGUI, Support Open Source software, 3
if ErrorLevel {
	Return
}
WinActivate, ahk_class AutoHotkeyGUI, Support Open Source software
Sleep, 20
ControlClick, ^No
Sleep, 20
WinWaitActive, ahk_class AutoHotkeyGUI, MacroCreator, 2
if ErrorLevel {
	Return
}
; all good
Sleep, 20

; Get ID of active window so we can refer to it later when there's no accessible handle
WinGet, macro_id, ID, A
Send, {Alt}+m+r
Sleep, 70
; Look for annoying pop-up window about how to record
IfWinExist, ahk_class AutoHotkeyGUI, Resources.dll
{
	WinActivate, ahk_class AutoHotkeyGUI, Resources.dll
	ControlClick, OK
	Sleep, 40
}

; Now to deal with the bar that's hovering
WinActivate, ahk_id %macro_id%
Sleep, 40
MouseClick, Right, 7, 3, 1, 0
Sleep, 40
Send, {Down}
Sleep, 10
Send, {Enter}
Sleep, 5
; Get rid of the tool tip
WinActivate, ahk_class tooltips_class
MouseClick, Left, 2, 2, 1, 0

; F10 to start recording
; F9 to stop recording

Return