﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices; // for COM visibility
using System.Windows.Forms; // for MessageBox
using ExcelDna.Integration;
using ExcelDna.Integration.CustomUI;

namespace BoltMacroRecording
{
    // Macros go here
    public partial class BoltoControl
    {
        internal static int taskIndex = 1;

        public static void NextTask()
        {
            PaneControl.SpawnPane(taskIndex);
            taskIndex++;
        }

        public static void Commence()
        {
            // to do
        }

        public static void Finish()
        {
            // to do
        }

        [ExcelFunction(IsHidden=true)]
        public static void Help() {
            MessageBox.Show("Ask someone else for help");
        }
    }

    [ComVisible(true)]
    public class BoltoRibbon : ExcelDna.Integration.CustomUI.ExcelRibbon
    {
        public override string GetCustomUI(string RibbonID)
        {
            // ensure two instances of Excel are running
            return BoltoRibbon.ribbonXML;
        }

        public void RibbonAction(IRibbonControl control)
        {
            dynamic app = ExcelDnaUtil.Application;
            if (app == null)
                return;
            try
            {
                app.Run(control.Tag);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString()); // oh no
            }
        }

        private static string ribbonXML = @"<customUI xmlns='http://schemas.microsoft.com/office/2006/01/customui'>
               <ribbon>
                 <tabs>
                   <tab idMso='TabHome'>
                     <group idMso='GroupFont' visible='false' />
                   </tab>
                   <tab id='CustomTab' label='❡♥'>
                     <group id='Core' label='Tasks' >
                        <button tag='NextTask' id='NextTask' label='Next Task' imageMso='TableDrawTable' size='large' onAction='RibbonAction'/>
                        <button tag='Commence' id='Commence' label='Commence Stage' imageMso='StartTimer' size='large' onAction='RibbonAction'/>
                        <button tag='Finish' id='Finish' label='Finish Stage' imageMso='FileMarkAsFinal' size='large' onAction='RibbonAction'/>
                        <button tag='Help' id='Help' label='❡♥' imageMso='TentativeAcceptInvitation' size='large' onAction='RibbonAction'/>
                     </group>
                   </tab>
                 </tabs>
               </ribbon>
             </customUI>";
    }
}
