﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelDna.Integration;
using ExcelDna.Integration.CustomUI;
using System.Windows.Forms.Integration; // for ElementHost
using System.Runtime.InteropServices; // for COM visibility
using System.IO; // for StreamReader
using System.Windows.Markup; // for XamlReader
using System.Xml;
using System.Xaml;
using System.Windows.Controls;

namespace BoltMacroRecording
{
    [ComVisible(true)]
    public class TaskPanel : System.Windows.Forms.UserControl
    {
        // hold a XAML pane
        private BoltMacroRecording.TaskUI content;

        public TaskPanel()
        {
            this.content = new TaskUI();

            ElementHost wpfElementHost = new ElementHost() { Dock = DockStyle.Fill };
            wpfElementHost.HostContainer.Children.Add(this.content);

            this.Controls.Add(wpfElementHost);
        }
    }
    
    [ComVisible(true)]
    internal static class PaneControl
    {
        private static CustomTaskPane currentPane;

        internal static void SpawnPane(int taskIndex)
        {
            // create the next pane, possibly closing old one
            if (currentPane != null)
            {
                currentPane.Delete();
                currentPane = null;
            }
            currentPane = CustomTaskPaneFactory.CreateCustomTaskPane(typeof(TaskPanel),
                String.Format("Task {0:d2}", taskIndex));
            currentPane.Visible = true;
            currentPane.DockPosition = MsoCTPDockPosition.msoCTPDockPositionFloating;

            PaneControl.ChangeTask(taskIndex);
        }

        internal static void ChangeTask(int taskIndex)
        {
            string filename = String.Format("Task {0:d}.xaml", taskIndex);
            if (File.Exists(filename))
            {
                try
                {
                    // open the next task
                    TextReader reader = File.OpenText(filename);
                    XmlReader xReader = XmlReader.Create(reader);
                    TextBlock taskText = (TextBlock)System.Windows.Markup.XamlReader.Load(xReader);

                    // get a handle to the window to apply this task
                    System.Windows.Forms.UserControl taskControl = (System.Windows.Forms.UserControl)currentPane.ContentControl;
                    System.Windows.Forms.Integration.ElementHost elm = (System.Windows.Forms.Integration.ElementHost)taskControl.GetNextControl(null, true);
                    System.Windows.Controls.UIElementCollection elmHostChildren = elm.HostContainer.Children;
                    TaskUI tuì = (TaskUI)elmHostChildren[0];
                    tuì.ReviseText(taskText);
                }
                catch (Exception e)
                {
                    MessageBox.Show(String.Format("Error while looking for task: {0:s}", e.ToString()));
                }
            }
            else
            {
                MessageBox.Show("No more tasks");
            }
        }

        internal static bool CheckTask(int taskIndex)
        {
            string filename = String.Format("Task {0:d}.xaml", taskIndex);
            return File.Exists(filename);
        }
    }
}
