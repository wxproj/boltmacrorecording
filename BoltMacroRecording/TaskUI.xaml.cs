﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup; // for XamlReader
using System.Xaml;
using System.Xml;

namespace BoltMacroRecording
{
    /// <summary>
    /// Interaction logic for TaskUI.xaml
    /// </summary>
    public partial class TaskUI : System.Windows.Controls.UserControl
    {
        private TaskText soul;
        
        public TaskUI()
        {
            InitializeComponent();
            soul = new TaskText();
            {
                soul.text="Change me";
            };
            placeHolderText.DataContext = soul;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(String.Format("You clicked the button for task {0:d}!",
                ((Button)sender).Tag));
        }

        internal void ReviseText(TextBlock taskText)
        {
            // by accepting a TextBlock, it's easy to bind more elements and change the
            // look of the window for each task
            this.soul = new TaskText();
            {
                soul.text = taskText.Text;
            }
            this.placeHolderText.DataContext = soul;
        }

    }

    internal class TaskText
    {
        public string text { get; set; }
    }
}
