#NoEnv

SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
DetectHiddenWindows On
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1

WinActivate, ahk_class Shell_TrayWnd
WinSet, Transparent, Off
WinGetPos, trayX, trayY, trayWidth, trayHeight, A
iconPath = C:\development\BoltMacroRecording\BoltMacroRecording\template.bmp
startSearch = %trayWidth%*(3/4)
if ErrorLevel > 0
	MsgBox Early problem
	Return
ImageSearch, FoundX, FoundY, 0, 0, startSearch, %trayHeight%, %iconPath%
if ErrorLevel = 2
	MsgBox Could not conduct the search
	Return
else if ErrorLevel = 1
	MsgBox Icon couldn't be found
	Return
else {
	; Show the full window
	MouseClick, Right, %FoundX%+4, %FoundY%+2, 1, 0
	Sleep, 40
	Send, {Up}
	Sleep, 3
	Send, {Up}
	Sleep, 3
	Send, {Enter}
	Sleep, 20
}
Send, {Alt}+f+e
WinWaitActive, ahk_class AutoHotkeyGUI, MacroCreator, Macros, 2
if ErrorLevel {
	Return
}
ControlClick, Edit3
Sleep, 5
Send, C:\development\BoltMacroRecording\BoltMacroRecording\❡♥.ahk
; sending these characters ensures that our filename is reserved
Sleep, 40
WinWaitActive, Exported, ucces, 5
if ErrorLevel {
	Return
}
ControlClick, OK
Sleep, 10
ControlClick, Close
Sleep, 10
Send, {Alt}+f+x
WinWaitActive, Save Macro,,2
ControlClick, No

Return